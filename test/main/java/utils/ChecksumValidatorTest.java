package main.java.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChecksumValidatorTest {

    ChecksumValidator validator = new ChecksumValidator();
    String VALID_CHECKSUM = "4242424242424242";
    String INVALID_CHECKSUM = "623456";
    String VALID_CARD_LENGTH = "7362537462537498";
    String INVALID_CARD_LENGTH = "1234132";

    @Test
    void shouldBeCorrectSumOfSumDigits() {
        // Input and expected sum of sum digits provided by: wikipedia.org/wiki/Luhn_algorithm
        assertEquals(67, validator.getSumDigits("79927398713"));
    }

    @Test
    void shouldBeCorrectCheckDigit() {
        // Method strips the provided check digit from the valid card input,
        // and computes the correct check digit based on sum digits
        assertEquals(2, validator.getComputedCheckDigit(VALID_CHECKSUM));
    }

    @Test
    void shouldBeIncorrectCheckDigit() {
        assertNotEquals(6, validator.getComputedCheckDigit(INVALID_CHECKSUM));
    }

    @Test
    void providedCheckDigitIsParsedCorrectly() {
        assertEquals(2, validator.getProvidedCheckDigit(VALID_CHECKSUM));
        assertNotEquals(3, validator.getProvidedCheckDigit(VALID_CHECKSUM));
    }

    @Test
    void inputConversionToIntArrayIsSuccessful() {
        int[] expected = {1,2,3,4,5};
        assertArrayEquals(expected, validator.inputToIntArr("12345"));
    }

    @Test
    void shouldBeIncorrectSumOfSumDigits() {
        assertNotEquals(67, validator.getSumDigits("79927"));
    }

    @Test
    void shouldBeValidChecksumAccordingToLuhn() {
        assertTrue(validator.isValidChecksum(VALID_CHECKSUM));
    }

    @Test
    void shouldBeInvalidChecksumAccordingToLuhn() {
        assertFalse(validator.isValidChecksum(INVALID_CHECKSUM));
    }

    @Test
    void shouldBeValidCardLength() {
        assertTrue(validator.isValidCardLength(VALID_CARD_LENGTH));
    }

    @Test
    void shouldBeInvalidCardLength() {
        assertFalse(validator.isValidCardLength(INVALID_CARD_LENGTH));
    }

}