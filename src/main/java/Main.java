package main.java;

import main.java.utils.ChecksumValidator;
import main.java.utils.ConsoleHandler;

public class Main {

    public static void main(String[] args) {
        ConsoleHandler.initMenu();
        String nums = ConsoleHandler.getNumericInput();
        ChecksumValidator validator = new ChecksumValidator();
        validator.validate(nums);
    }
}
