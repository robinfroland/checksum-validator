package main.java.utils;

public class ChecksumValidator {

    public void validate(String input) {
        String checksumResults =  isValidChecksum(input) ? "Valid" : "Invalid";
        String digitResults = isValidCardLength(input)
                ? input.length() + " (credit card)"
                : Integer.toString(input.length());

        System.out.println("Input: " + getFormattedInput(input));
        System.out.println("Provided: " + getProvidedCheckDigit(input));
        System.out.println("Expected: " + getComputedCheckDigit(input));

        System.out.println("\nChecksum: " + checksumResults);
        System.out.println("Digits: " + digitResults);
    }

    public int getSumDigits(String input) {
        int[] digits = inputToIntArr(input);
        boolean shouldDouble = true;
        int sumDigits = 0;

        // Skip provided check digit. Alternate between digits.
        for (int i = digits.length-2; i >= 0; i--) {
            int val = digits[i];
            if (shouldDouble) {
                val *= 2;
                if (val > 9) {
                    val = 1 + (val % 10);
                }
            }
            sumDigits += val;
            shouldDouble = !shouldDouble;
        }
        return sumDigits;
    }

    public boolean isValidChecksum(String input) {
        // Add provided check digit to sum digits in order to verify checksum
        int checksum = getSumDigits(input) + getProvidedCheckDigit(input);
        return checksum % 10 == 0;
    }

    public boolean isValidCardLength(String input) {
        return input.length() == 16;
    }

    public int getComputedCheckDigit(String input) {
        int sumDigits = getSumDigits(input);
        return (sumDigits * 9) % 10;
    }

    public Integer getProvidedCheckDigit(String input) {
        return Integer.parseInt(input.substring(input.length()-1));
    }

    public int[] inputToIntArr (String input) {
        int[] intArray = new int[input.length()];

        for (int i = 0; i < input.length(); i++) {
            intArray[i] = Integer.parseInt(input.substring(i, i+1));
        }

        return intArray;
    }

    private String getFormattedInput(String input) {
        return input.substring(0, input.length()-1) + " " + getProvidedCheckDigit(input);
    }

}

