package main.java.utils;

import java.util.Scanner;

public class ConsoleHandler {
    private static Scanner input = new Scanner(System.in);

    public static void initMenu() {
        outputHeader();
    }

    public static String getNumericInput() {
        boolean isNumeric = false;
        String inputString = "";

        System.out.println("Please enter a sequence of numbers:");

        while (!isNumeric) {
            System.out.print(" > ");
            inputString = input.nextLine();
            if (inputString.matches("[0-9]+")) {
                isNumeric = true;
            } else {
                System.out.println("Enter digits only! Try again.");
            }
        }
        return inputString;
    }

    private static void outputHeader() {
        System.out.println("+-----------------------------------------+");
        System.out.println("|           CHECKSUM VALIDATOR            |");
        System.out.println("+-----------------------------------------+");
    }
}
